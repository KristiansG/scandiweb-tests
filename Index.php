<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Product List</title>
    <link rel="stylesheet" type="text/css" href="style.css">    
</head>
<body>

<h1>Product List
    <tr class="dropDownList">
        <td>
            <select class="optionList" name="Options">
                <option value="">Mass Delete Action</option>
                <option value="Option"
                <?php
                if(isset($_POST['Options']) && $_POST['Options']=="Option") 
                { ?>selected<?php  } ?>>Select None</option>

                <option value="secondOption" 
                <?php 
                if(isset($_POST['Options']) && $_POST['Options']=="secondOption") 
                { ?>selected<?php  } ?>>Select All</option>
            </select>
        </td>
    <input class="applyButton" type="button" value="Apply" oncilck="">
    </tr>
</h1>

<tr class="product-row">
    <div> 
        <div class="productOne">
        <input type="checkbox" class="checkBox" id="checkBox1">       
        <p>JVC100123</p>
        <p>Acme Disc</p>
        <p>1.00$</p>
        <p>Size 700MB</p>
        </div>
    </div>
    <div class="productOne">
        <input type="checkbox" class="checkBox" id="checkBox2">
        <p>JVC200123</p>
        <p>Acme Disc</p>
        <p>1.00$</p>
        <p>Size 700MB</p>
    </div>
    <div class="productOne">
        <input type="checkbox" class="checkBox" id="checkBox3">
        <p>JVC300123</p>
        <p>Acme Disc</p>
        <p>1.00$</p>
        <p>Size 700MB</p>
    </div>
    <div class="productOne">
        <input type="checkbox" class="checkBox" id="checkBox4">        
        <p>JVC400123</p>
        <p>Acme Disc</p>
        <p>1.00$</p>
        <p>Size 700MB</p>
    </div>
</tr>

<tr class="productSecond-row">
    <div class="productTwo">
        <input type="checkbox" class="checkBox" id="checkBox5">
        <p>GGWP0001</p>
        <p>War and Peace</p>
        <p>20.00$</p>
        <p>Weight 2kg</p>
    </div>
    <div class="productTwo">
        <input type="checkbox" class="checkBox" id="checkBox6">
        <p>GGWP0001</p>
        <p>War and Peace</p>
        <p>20.00$</p>
        <p>Weight 2kg</p>
    </div>
    <div class="productTwo">
        <input type="checkbox" class="checkBox" id="checkBox7">
        <p>GGWP0001</p>
        <p>War and Peace</p>
        <p>20.00$</p>
        <p>Weight 2kg</p>
    </div>
    <div class="productTwo">
        <input type="checkbox" class="checkBox" id="checkBox8">
        <p>GGWP0001</p>
        <p>War and Peace</p>
        <p>20.00$</p>
        <p>Weight 2kg</p>
    </div>
</tr>

<tr class="productThird-row">
    <div class="productThree">
        <input type="checkbox" class="checkBox" id="checkBox9">
        <p>TR120555</p>
        <p>Chair</p>
        <p>40.00$</p>
        <p>Dimenson 24x25x15</p>
    </div>
    <div class="productThree">
        <input type="checkbox" class="checkBox" id="checkBox10">
        <p>TR120666</p>
        <p>Chair</p>
        <p>40.00$</p>
        <p>Dimenson 24x25x15</p>
    </div>
    <div class="productThree">
        <input type="checkbox" class="checkBox" id="checkBox11">
        <p>TR120777</p>
        <p>Chair</p>
        <p>40.00$</p>
        <p>Dimenson 24x25x15</p>
    </div>
    <div class="productThree">
        <input type="checkbox" class="checkBox" id="checkBox12">
        <p>TR120888</p>
        <p>Chair</p>
        <p>40.00$</p>
        <p>Dimenson 24x25x15</p>
    </div>
</tr>

</body>
</html>